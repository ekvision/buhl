import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../../pages/user-details/user-details.component";

@Injectable({
	providedIn: 'root'
})
export class UserListService {

	constructor(private http: HttpClient) {
	}


	getAllUsers(): Observable<{users: User[]}> {
		return this.http.get<{users: User[]}>(`https://dummyjson.com/users`)
	}
}
