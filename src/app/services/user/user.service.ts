import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../../pages/user-details/user-details.component";

@Injectable({
	providedIn: 'root'
})
export class UserService {

	constructor(private http: HttpClient) {
	}


	createUser(userData: User): Observable<User> {
		return this.http.post<User>(`https://dummyjson.com/users/add`, userData,
			{
				headers: {'Content-Type': 'application/json'}
			})
	}


	getOwnUser(id: number): Observable<User> {
		return this.http.get<User>(`https://dummyjson.com/users/${id}`)
	}


	updateUser(userData: User, id: number): Observable<User> {
		return this.http.put<User>(`https://dummyjson.com/users/${id}`, {userData}, {
			headers: {'Content-Type': 'application/json'}
		})
	}


	deleteUser(id: number): Observable<User> {
		return this.http.delete<User>(`https://dummyjson.com/users/${id}`, {
			headers: {'Content-Type': 'application/json'}
		})
	}
}
