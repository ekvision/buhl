import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserListService} from "../../services/user-list/user-list.service";
import {Subject, takeUntil} from "rxjs";
import {User} from "../user-details/user-details.component";
import {UserService} from "../../services/user/user.service";

@Component({
	selector: 'app-user-list',
	templateUrl: './user-list.component.html',
	styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, OnDestroy{

	users: User[] = [];
	unsubscribe$:Subject<void> = new Subject<void>();


	constructor(
		public userListService: UserListService,
		public userService: UserService
	) {}


	ngOnInit(): void {
		// Get all exists user.
		this.userListService.getAllUsers().pipe(takeUntil(this.unsubscribe$)).subscribe({
			next: (data: {users: User[]} ) => {
				this.users = data.users;
			},
			error: error => console.log(error)
		})
	}


	// Delete user from users list
	deleteUser(userId: number) {
		this.userService.deleteUser(userId)
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe({
				next: (user: User) => {
					if(user.isDeleted) this.users = this.users.filter(i => i.id !== user.id);
				},
				error: (err) => console.log(err)
			})
	}


	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}
}
