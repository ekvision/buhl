import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../services/user/user.service";
import {Subject, takeUntil} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";


export interface User {
	id: number;
	firstName: string;
	lastName: string;
	birthDate: string; //(Format DD.MM.YYYY)
	email: string;
	gender: string;
	image: string;
	isDeleted?: boolean
}

@Component({
	selector: 'app-user-details',
	templateUrl: './user-details.component.html',
	styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit, OnDestroy {

	userForm: FormGroup = new FormGroup({
		firstName: new FormControl(null, [
			Validators.required,
			Validators.minLength(2),
			Validators.maxLength(40),
			Validators.pattern(/^[a-zA-Z]+[\s]?[a-zA-Z]*$/)
		]),
		lastName: new FormControl(null, [
			Validators.required,
			Validators.minLength(2),
			Validators.maxLength(40),
			Validators.pattern(/^[a-zA-Z]+[\s]?[a-zA-Z]*$/)
		]),
		birthDate: new FormControl(null, Validators.required),
		// Validierung für email wurde nach eigen Regexp gemacht da von Angular arbeitet falsch.
		email: new FormControl(null, [
			Validators.required,
			Validators.pattern(/^(([^<>()[\]\\.,;:№\s@"]+(\.[^<>()[\]\\.,;:№\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
		]),
		gender: new FormControl(null, Validators.required),
	});
	unsubscribe$: Subject<void> = new Subject<void>();
	viewDetails: boolean = false;

	// Begrenzung von maximal mögliche Datum Eingabe.
	maxDate: string = new Date().toISOString().split("T")[0];
	@ViewChild('inputBirthDate') inputBirthDate: ElementRef | undefined;


	constructor(
		private userService: UserService,
		private route: ActivatedRoute,
		private router: Router
	) {
		// Wenn wir uns auf Seite  user-details befinden, wird das form disabled,
		// und entsprechende Styling verwendet.
		if(router.url.includes('details')) {
			this.viewDetails = true;
			this.userForm.disable();
		}
	}


	ngOnInit(): void {
		// wenn id parameter in router vorhanden ist, wird eine Anfrage gemacht um ein User zu bekommen.
		if(this.route.snapshot.params['id']) {
			this.userService.getOwnUser(Number(this.route.snapshot.params['id']))
				.pipe(takeUntil(this.unsubscribe$))
				.subscribe({
					next: (res) => {
						this.userForm.setValue({
							firstName: res.firstName,
							lastName: res.lastName,
							birthDate: res.birthDate,
							email: res.email,
							gender: res.gender
						})
					},
					error: err => console.log(err)
				})
		}
	}


	// per click auf date input, DatePicker zu öffnen.
	showPicker() {
		this.inputBirthDate?.nativeElement.showPicker()
	}


	//getter for validation
	get firstName() {
		return this.userForm.get('firstName')
	}


	//getter for validation
	get lastName() {
		return this.userForm.get('lastName')
	}


	//getter for validation
	get birthDate() {
		return this.userForm.get('birthDate')
	}


	//getter for validation
	get email() {
		return this.userForm.get('email')
	}


	//getter for validation
	get gender() {
		return this.userForm.get('gender')
	}


	// Wenn wir uns auf Seite  user-create befinden wird function aufgerufen um einen neuen User anzulegen,
	// sonst user zu updaten
	formSubmit() {
		if(this.router.url.includes('create')) {
			this.userService.createUser(this.userForm.value)
				.pipe(takeUntil(this.unsubscribe$))
				.subscribe({
					next: res => {
						this.userForm.reset();
						this.router.navigateByUrl('/');
					},
					error: error => {
						console.log(error)
					}
				})
		} else {
			this.userService.updateUser(this.userForm.value, Number(this.route.snapshot.params['id']))
				.pipe(takeUntil(this.unsubscribe$))
				.subscribe({
					next: res => {
						this.userForm.reset();
						this.router.navigateByUrl('/');
					},
					error: error => {
						console.log(error)
					}
				})
		}
	}


	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}
}
