# Buhl

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

Webseite für eine Userverwaltung.

Design hat responsives Verhalten.

Es ist möglich:
    • Anlegen von neuen Personen
    • Anzeigen von bestehenden Personen in einer Liste
    • Entfernen von Personen aus der Liste
    • Editieren bereits vorhandener Personen

